volatile unsigned char result;

ISR(ADC_vect){
  result = ADCH;
}

int main(void){
  DDRD = 0xFF;
  sei();
  aditionnal_part();
  mandatory_part();
}

int mandatory_part(void){
  ADMUX = 0b00100000;
  ADCSRA = 0b10000111;
  TCCR0A = (1 << WGM00);
  TCCR0B = (1 << CS00);
  OCR0B = 0xFF/128;
  TCCR0A |= (1<<COM0B1);
  	while(1){
      ADCSRA |= 1<<ADSC;
      while(ADCSRA & (1 << ADSC)){};
      result =ADCH; 
      PORTD = result;
    }
}

int aditionnal_part(void){
  	ADMUX = 0b00100011;
  	ADCSRA = 0b10001110;
  	while(1){
      ADCSRA |= (1 << ADSC);
      PORTD = result;
    }
}